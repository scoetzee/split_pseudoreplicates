#!/bin/bash
set -e

function bam2bed {
    if [[ -z "$1" ]] ; then
	echo "argument has length zero"
	return 1
    fi
    if [[ ${1: -3} == "bam" ]] ; then
	echo "converting ${1} from bam file to tagAlign.gz"
	samtools view -b -F 1548 -q 20 ${1} | \
	    bamToBed -i stdin | \
	    awk 'BEGIN{FS="\t";OFS="\t"}{$4="N"; print $0}' | \
	    gzip -c > ${1%.bam}.tagAlign.gz
    else
	echo "argument is not a bam file"
	return 1
    fi
}
export -f bam2bed

function createPseudoreplicates {
    if [[ ${1: -3} == "bam" ]] ; then
	if [[ -e ${1%.bam}.tagAlign.gz ]] ; then
	    Reads=${1%.bam}.tagAlign.gz
	else
	    bam2bed $1
	    Reads=${1%.bam}.tagAlign.gz
	fi 
    else
	Reads="$1"
    fi
    Tissue="$2"
    Factor="$3"
    Condition="$4"
    Treatment="$5"
    OUTDIR="/data/output/macs2/${Tissue}_${Factor}_${Condition}_${Treatment}/pseudoreps"
    mkdir -p $OUTDIR
    OUTSTUB="$OUTDIR/$(basename ${Reads%.tagAlign.gz}_pr)"
    echo "creating pseudoreplicates ${OUTSTUB}1.tagAlign.gz"
    echo "and ${OUTSTUB}2.tagAlign.gz"
    nlines=$( zcat ${Reads} | wc -l )
    nlines=$(( (nlines + 1) / 2 ))
    zcat $Reads | shuf | split -d -l ${nlines} - "${OUTSTUB}"
    gzip -c ${OUTSTUB}00 > ${OUTSTUB}1.tagAlign.gz && rm ${OUTSTUB}00
    gzip -c ${OUTSTUB}01 > ${OUTSTUB}2.tagAlign.gz && rm ${OUTSTUB}01
}
export -f createPseudoreplicates

MANIFEST=/data/manifest.csv
IFS=","

if [[ -e $MANIFEST ]] ; then
	echo "starting with split and pool ... "
else
	echo "directory does not contain manifest file"
	exit 1
fi

TOTALLINES=$(cat $MANIFEST | wc -l)
TOTALSAMPLES=$(cut -f1 -d',' $MANIFEST | sort -u | wc -l)

if [[ $TOTALSAMPLES -ne $TOTALLINES ]] ; then
    echo "not all sample names are unique... quitting"
    exit 1
fi

cd /data
## lets find replicates!
sed '1d' $MANIFEST | while read SampleID Tissue Factor Condition Treatment Replicate Reads Input; do
    createPseudoreplicates ${Reads} ${Tissue} ${Factor} ${Condition} ${Treatment}
    if [[ $Replicate -eq 1 ]] ; then
	OUTDIR="/data/output/macs2/${Tissue}_${Factor}_${Condition}_${Treatment}"
	POUTFILE="${OUTDIR}/${Tissue}_${Factor}_${Condition}_${Treatment}_pooledreads.tagAlign.gz"
	if [[ -e $POUTFILE ]] ; then
	    rm $POUTFILE
	fi
	unset IFS
	cat $(grep "$Tissue,$Factor,$Condition,$Treatment" ${MANIFEST} | cut -f7 -d",") >> $POUTFILE
	IFS=","
	createPseudoreplicates ${POUTFILE} ${Tissue} ${Factor} ${Condition} ${Treatment}
    fi
done
echo "completed run, all files generated"
exit 0
