#!/usr/bin/env cwl-runner
#
# Author: Simon.Coetzee@csmc.edu

class: Workflow

cwlVersion: "cwl:draft-3.dev3"

description:
  splits tagAlign.gz files into pseudoreplicates

inputs:
  - id: SampleID
    type: ["null", string]

  - id: Tissue
    type: ["null", string]

  - id: Factor
    type: ["null", string]

  - id: Condition
    type: ["null", string]

  - id: Treatment
    type: ["null", string]

  - id: Replicate
    type: ["null", int]

  - id: Reads
    type: File

  - id: Input
    type: File

outputs:
  - id: pseudorep1
    type: File

  - id: pseudorep2
    type: File

steps:
  - id: 